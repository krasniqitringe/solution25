"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var mongoose_unique_validator_1 = __importDefault(require("mongoose-unique-validator"));
var ProductSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true,
        lowercase: true,
        unique: true
    },
    description: {
        type: String,
        required: true,
        lowercase: true
    }
});
ProductSchema.plugin(mongoose_unique_validator_1.default, { message: 'is already taken.' });
exports.default = mongoose_1.model('Product', ProductSchema);
