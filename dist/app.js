"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var morgan_1 = __importDefault(require("morgan"));
//Routes
var routes_1 = __importDefault(require("./routes"));
var products_1 = __importDefault(require("./routes/products"));
var Applicaction = /** @class */ (function () {
    function Applicaction() {
        this.app = express_1.default();
        this.settings();
        this.middlewares();
        this.routes();
    }
    Applicaction.prototype.settings = function () {
        this.app.set('port', 5000);
    };
    Applicaction.prototype.middlewares = function () {
        this.app.use(morgan_1.default('dev'));
        this.app.use(express_1.default.urlencoded({ extended: false }));
        this.app.use(express_1.default.json());
    };
    Applicaction.prototype.routes = function () {
        this.app.use('/', routes_1.default);
        this.app.use('/api', products_1.default);
    };
    Applicaction.prototype.start = function () {
        var _this = this;
        this.app.listen(this.app.get('port'), function () {
            console.log('Server is running at', _this.app.get('port'));
        });
    };
    return Applicaction;
}());
exports.default = Applicaction;
