import { Schema, model } from 'mongoose';
import uniqueValidator  from 'mongoose-unique-validator';

const ProductSchema = new Schema({
    name: {
        type: String,
        required: true,
        lowercase: true,
        unique: true
    },
    description: {
        type: String,
        required: true,
        lowercase: true
    }
});

ProductSchema.plugin(uniqueValidator, {message: 'is already taken.'});
export default model('Product', ProductSchema);
