import { Router, Request, Response } from 'express';
const router = Router();

//Model
import Product from '../models/product';

//Create a new product
router.route('/products/create')
    .post(async (req: Request, res: Response) => {
        try {
            const { name, description } = req.body;
            const product = new Product({ name, description });
            await product.save();
            res.json({"message":"Product was successfuly created!"})
        } catch (error) {
            res.json({"message": error.message})
        }
    });

//List all products
router.route('/products/list')
    .get(async (req: Request, res: Response) => {
        try {
            const products = await Product.find();
            res.send(products)
        } catch (error) {
            res.json({"message": error.message});
        }
    }); 

//Edit a product
router.route('/products/edit/:id')
.post(async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const { name, description } = req.body;
        await Product.findByIdAndUpdate(id, {
            name, description
        });
        res.json({"message":"Product was successfuly edited"})
    } catch (error) {
        res.json({"message": error.message});
    }
})

//Delete a product 
router.route('/products/delete/:id')
    .delete(async (req: Request, res: Response) => {
        try {
            const { id } = req.params;
            await Product.findByIdAndDelete(id);
            res.json({"message":"Product was successfuly deleted"});
        } catch (error) {
            res.json({"message": error.message});
        }
    });



export default router;